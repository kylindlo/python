import psycopg2
import os

# Connect to database
cursor = psycopg2.connect(os.environ['DATABASE_URL'], sslmode='require').cursor()
#cursor = psycopg2.connect(host='localhost', dbname='kulyindlo', user='test_user',
#                          password='qwerty').cursor()

# Open file with corpus
with open('taskO1.txt', 'r') as f:
    verse = ''
    is_text = False
    for line in f:
        if line.strip().startswith('МЕТКА'):
            continue
        elif line.startswith('НАЗВАНИЕ'):
            title = line.replace('НАЗВАНИЕ', '').strip()
            if (title.isnumeric() or title == 'No one cares'):
                #verse += title + '\n'
                title = ''
            #verse += '\n'
        elif line == 'ТЕКСТ\n':
            is_text = True
            continue
        elif line == 'КОН\n':
            is_text = False
            # Add verse to table verses
            #cursor.execute('insert into verses (verse) values (\''
            #               + verse + '\')')
            cursor.execute("""insert into poems
                           (name, text, created_at, updated_at, admin_user_id)
                           values ('{name}', '{text}',
                           current_timestamp, current_timestamp, 0)"""
                                   .format(name=title, text=verse))
            verse = ''
        else:
            if is_text:
                verse += line

cursor.execute('commit')
cursor.close()