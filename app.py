import flask
import psycopg2
import gensim
import pymystem3
import json
import os

app = flask.Flask(__name__)

# Возвращает список текстов, загруженных из базы данных
def load_texts_from_database(cursor):
    # Get texts from database
    #cursor.execute('select verse from verses')
    cursor.execute('select name, text from poems')
    data = cursor.fetchall()
    texts = []
    for text in data:
        if text[0] == '':
            texts.append('\n' + text[1])
        else:
            texts.append(text[0] + '\n\n' + text[1])
    
    return texts

# Функция преобразует список стихов в список списков токенов вида лемма_ЧАСТЬРЕЧИ
# Классификация частей речи в формате Mystem
# Возможен учёт стоп-слов
def preprocess(texts, stop_words=None):
    mystem = pymystem3.Mystem()
    preprocessed_texts = []
    
    for text in texts:
        preprocessed_text = []
        analized = mystem.analyze(text)
        
        for result in analized:
            if 'analysis' not in result:
                continue
            if result['analysis'] != []:
                lemma = result['analysis'][0]['lex']
            if stop_words is not None:
                if lemma in stop_words:
                    continue
            if result['analysis'] != []:
                pos = result['analysis'][0]['gr'].split(',')[0].split('=')[0]
            else:
                lemma = result['text']
                pos = 'NONE'
            preprocessed_text.append(lemma.lower() + '_' + pos)
            
        preprocessed_texts.append(preprocessed_text)
        
    return preprocessed_texts

# Создаёт и возвращает обученную модель векторизованных токенов
def create_model(preprocessed_texts, size=150, window=15, min_count=1, epochs=15):
    model = gensim.models.Word2Vec(preprocessed_texts, size=size,
                                   window=window, min_count=min_count)
    filtered_preprocessed_texts = []
    for preprocessed_text in preprocessed_texts:
        filtered_preprocessed_texts.append(list(filter(
                lambda token: not token.endswith('NONE'),
                preprocessed_text)))
    model.train(filtered_preprocessed_texts,
                total_examples=len(filtered_preprocessed_texts),
                    epochs=epochs)
    return model

# Возвращает список текстов из texts, которые удовлетворяют запросу по слову word
# topn - количество "похожих" слов, также используемых при поиске
# log - показать список слов, использвуемых при поиске (слово + похожие)
def search(word, model, texts, preprocessed_texts, topn=10, log=False):
    try:
        if model is None:
            raise KeyError('No model')
        mystem = pymystem3.Mystem()
        analyzed = mystem.analyze(word)
        if 'analysis' not in analyzed[0]:
            raise KeyError('Incorrect search queue')
        if analyzed[0]['analysis'] != []:
            lemma = analyzed[0]['analysis'][0]['lex']
            pos = analyzed[0]['analysis'][0]['gr'].split(',')[0].split('=')[0]
        else:
            lemma = analyzed[0]['text']
            pos = 'NONE'
        word = lemma + '_' + pos
        if log:
            print('Ищем: ' + word + ' ...')

        searched_texts = set()
        for i in range(len(preprocessed_texts)):
            if word in preprocessed_texts[i]:
                searched_texts.add(texts[i])
        if topn > 0:
            if log:
                print('А также:')
            for result in model.wv.most_similar(word, topn=topn):
                similar_word = result[0]
                if log:
                    print(similar_word)
                for i in range(len(preprocessed_texts)):
                    if similar_word in preprocessed_texts[i]:
                        searched_texts.add(texts[i])
            if log:
                print()
        return list(searched_texts)
    except KeyError:
        return ['Поиск не дал результатов']

# Возвращает список слов, наиболее похожих на слово word
def similar(word, model, topn=10):
    try:
        if model is None:
            raise KeyError('No model')
        mystem = pymystem3.Mystem()
        analyzed = mystem.analyze(word)
        if 'analysis' not in analyzed[0]:
            raise KeyError('Incorrect search queue')
        if analyzed[0]['analysis'] != []:
            lemma = analyzed[0]['analysis'][0]['lex']
            pos = analyzed[0]['analysis'][0]['gr'].split(',')[0].split('=')[0]
        else:
            lemma = analyzed[0]['text']
            pos = 'NONE'
        word = lemma + '_' + pos
        
        tokens = list(map(lambda word_metrics: word_metrics[0],
                          model.most_similar(word, topn=topn)))
        return list(map(lambda token: token.split('_')[0], tokens))
    except KeyError:
       return ['Поиск не дал результатов']

# Дообучает модель новыми списками токенов
def update_model(model, preprocessed_texts, cursor, epochs=15):
    model.build_vocab(preprocessed_texts, update=True)
    filtered_preprocessed_texts = []
    for preprocessed_text in preprocessed_texts:
        filtered_preprocessed_texts.append(list(filter(
                lambda token: not token.endswith('NONE'),
                preprocessed_text)))
    model.train(filtered_preprocessed_texts,
                total_examples=len(filtered_preprocessed_texts),
                epochs=epochs)

# Добавляет в базу данных новые тексты
def update_database(cursor, texts):
    for text in texts:
        cursor.execute('insert into verses (verse) values (\''
                           + text + '\')')
    cursor.execute('commit')

# Установим соединение с базой данных
cursor = psycopg2.connect(os.environ['DATABASE_URL'],
                          sslmode='require').cursor()
#cursor = psycopg2.connect(host='localhost', dbname='kulyindlo', user='test_user',
#                         password='qwerty').cursor()

# При запуске сервера загружаем модель или, если её нет, создаём и обучаем
verses = load_texts_from_database(cursor)
preprocessed_verses = preprocess(verses)
model = None
#if os.path.isfile('./model'):
#    model = gensim.models.Word2Vec.load('model')
#else:
#    if (verses != []):
#        model = create_model(preprocessed_verses)
#        model.save('model')
if (verses != []):
    model = create_model(preprocessed_verses)

# Ищет тексты по запросу ?q= и выдаёт результат в формате json
@app.route('/search')
def handle_search():
    query = flask.request.args.get('q')
    platform = flask.request.args.get('platform')
    if query == None or query == '':
        return ''
    result = search(query, model, verses, preprocessed_verses, topn=1)
    formatted_result = dict(poems=[])
    if platform == '1':
        #return json.dumps(result, ensure_ascii=False)
        if result == ['Поиск не дал результатов']:
            return json.dumps(formatted_result)
        for text in result:
            if text[:text.index('\n')] == '':
                formatted_result['poems'].append(dict(
                        name='',
                        text=text[text.index('\n')+1:]))
            else:
                formatted_result['poems'].append(dict(
                        name=text[:text.index('\n')],
                        text=text[text.index('\n', text.index('\n')+1):]))
        return json.dumps(formatted_result, ensure_ascii=False)
    else:
        text = ''
        for verse in result:
            text += verse.strip().replace('\n', '<br>') + '<br>======<br>'
        return text

# Возвращает список слов, наиболее похожих на слово из запроса
@app.route('/similar')
def handle_similar():
    query = flask.request.args.get('q')
    topn = flask.request.args.get('topn')
    if topn is None:
        topn = 10
    platform = flask.request.args.get('platform')
    if query == None:
        return ''
    result = similar(query, model, topn=topn)
    if platform == '1':
        return json.dumps(result, ensure_ascii=False)
    else:
        text = ''
        for word in result:
            text += word + '<br>'
        return text

# Добавляет новый текст в модель и дообучает её
@app.route('/update', methods=['POST'])
def handle_update():
    global model
    if flask.request.method != 'POST':
        return ''
    
    no_verses = verses == []
    
    # Получаем текст стиха из формы запроса по ключу 'text'
    verse = flask.request.form['text']
    
    # Разбиваем на токены, лемматизируем и размечаем
    preprocessed_verse = preprocess([verse])[0]
    verses.append(verse)
    preprocessed_verses.append(preprocessed_verse)
    
    # Обновляем базу данных (больше нет)
    #update_database(cursor, [verse])
    # Если до этого база была пустой, то создаём модель
    if no_verses:
        model = create_model(preprocessed_verses)
    else:
        update_model(model, [preprocessed_verse], cursor)
    
    # Сохраняем модель
    return 'Success!'

# Возвращает корпус текстов
@app.route('/corpus')
def handle_corpus():
    platform = flask.request.args.get('platform')
    tokenized = flask.request.args.get('tokenized')
    if tokenized == '1':
        text = ''
        if platform == '1':
            return json.dumps(preprocessed_verses, ensure_ascii=False)
        else:
            text = ''
            for preprocessed_verse in preprocessed_verses:
                for token in preprocessed_verse:
                    text += token + ', '
                text = text[:-1] + ';<br>'
            return text
    else:
        if platform == '1':
            return json.dumps(verses, ensure_ascii=False)
        else:
            text = ''
            for verse in verses:
                text += verse.strip().replace('\n', '<br>') + '<br>======<br>'
            return text