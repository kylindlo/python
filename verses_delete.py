import psycopg2
import os

# Connect to database
cursor = psycopg2.connect(os.environ['DATABASE_URL'], sslmode='require').cursor()
#cursor = psycopg2.connect(host='localhost', dbname='kulyindlo', user='test_user',
#                          password='qwerty').cursor()

# Delete all from verses table
#cursor.execute('delete from verses')
cursor.execute('delete from poems')

cursor.execute('commit')
cursor.close()